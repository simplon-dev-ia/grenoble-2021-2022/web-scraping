![Logo](webscraping.jpg)

# Web Scraping

> Introduction à l'extraction de données par web-scraping

## Recherche d'information

- [ ] En quoi consiste le _web scraping_ ?
- [ ] Citer quelques cas d'utilisation concrets du web scraping ?
- [ ] Quelles bibliothèques Python permettent de faire du web scraping ?
- [ ] Quelles technologies / syntaxes permettent de sélectionner un contenu précis dans une page web ?
- [ ] Est-il possible de _scraper_ du contenu généré dynamiquement par une page web (JavaScript, CAPTCHA) ?
- [ ] Une application web peut-elle empêcher un robot de scraper son contenu ?
- [ ] La pratique du web scraping est-elle légale ? (en France, en Europe, dans le monde)
